unit uMainForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls,
  Menus, uEditorFrame, uCommandProcessor, uEditProcessor, uFileCommandProcessor, uCommandUtils, uActiveDocument;

type

  { TMainForm }

  TCustomTabSheet = Class(TTabSheet)
    private
      _editorFrame : TEditorFrame;

    public
      constructor Create(sheetOwner : TPageControl);
    public
      property EditorFrame : TEditorFrame read _editorFrame;
  end;

  TMainForm = class(TForm)
    CenterPanel: TPanel;
    EditorsPageControl: TPageControl;
    FileMenuItem: TMenuItem;
    FileNewItem: TMenuItem;
    FileOpenItem: TMenuItem;
    MenuBar: TMainMenu;
    EditMenuItem: TMenuItem;
    EditUndoItem: TMenuItem;
    EditRedoItem: TMenuItem;
    FileSaveItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    FileCloseItem: TMenuItem;
    DebugMenuItem: TMenuItem;
    DebugTestItem: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    FileExitItem: TMenuItem;
    NewPageTab: TTabSheet;
    TitleUpdateTimer: TTimer;
    TopPanel: TPanel;
    procedure DebugTestItemClick(Sender: TObject);
    procedure EditorsPageControlChange(Sender: TObject);
    procedure EditorsPageControlMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EditRedoItemClick(Sender: TObject);
    procedure EditUndoItemClick(Sender: TObject);
    procedure FileCloseItemClick(Sender: TObject);
    procedure FileNewItemClick(Sender: TObject);
    procedure FileOpenItemClick(Sender: TObject);
    procedure FileSaveAsItemClick(Sender: TObject);
    procedure FileSaveItemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure TitleUpdateTimerTimer(Sender: TObject);
  private
    procedure NewDocument();
  public
    function ShowSaveDialog() : string;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{$REGION 'Private Methods'}

procedure TMainForm.NewDocument();
var
  newSheet : TCustomTabSheet;
  newEditorFrame : TEditorFrame;
begin
  newEditorFrame := TEditorFrame.Create(nil);
  newSheet := TCustomTabSheet.Create(EditorsPageControl);
  newSheet.Caption := 'New';
  EditorsPageControl.ActivePage := newSheet;
  NewPageTab.PageIndex := EditorsPageControl.PageCount - 1; ;
end;

{$ENDREGION}

{$REGION 'Public Methods'}

function TMainForm.ShowSaveDialog() : string;
var
  dialog : TSaveDialog;
begin
  dialog := TSaveDialog.Create(self);
  if dialog.Execute then begin
    Result := dialog.FileName;
  end
  else begin
    Result := '';
  end;
end;

{$ENDREGION}

{$REGION 'Event Handlers'}

procedure TMainForm.FormCreate(Sender: TObject);
begin
  NewDocument();
end;

procedure TMainForm.FormKeyPress(Sender: TObject; var Key: char);
begin

end;

procedure TMainForm.TitleUpdateTimerTimer(Sender: TObject);
begin
  if EditorsPageControl.ActivePage is TCustomTabSheet then begin
    text := 'QEdit - ' + (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.LongTitle;
    EditorsPageControl.ActivePage.Caption := (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.ShortTitle;
  end;
end;

procedure TMainForm.EditorsPageControlChange(Sender: TObject);
begin
  if EditorsPageControl.ActivePage = NewPageTab then begin
    //EditorsPageControl.ActivePage := EditorsPageControl.Pages[EditorsPageControl.PageCount - 2];
  end;
end;

procedure TMainForm.DebugTestItemClick(Sender: TObject);
begin
  (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.Test();
end;

procedure TMainForm.EditorsPageControlMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if EditorsPageControl.ActivePage = NewPageTab then begin
    NewDocument();
  end;
end;

{$REGION 'Menu Events'}

procedure TMainForm.EditUndoItemClick(Sender: TObject);
begin
  if EditorsPageControl.ActivePage is TCustomTabSheet then begin
    (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.RunCommand(TTopLevelCommands.Edit + '.' + TEditCommands.Undo);
  end;
end;

procedure TMainForm.FileCloseItemClick(Sender: TObject);
var
  commandResult : TCommandResult;
begin
  if EditorsPageControl.ActivePage is TCustomTabSheet then begin
    commandResult := (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.RunCommand(TTopLevelCommands.FileCmd + '.' + TFileCommands.Close);
    if commandResult.Success then begin
      EditorsPageControl.ActivePage.Free();
    end;
  end;
end;

procedure TMainForm.EditRedoItemClick(Sender: TObject);
begin
  if EditorsPageControl.ActivePage is TCustomTabSheet then begin
    (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.RunCommand(TTopLevelCommands.Edit + '.' + TEditCommands.Redo);
  end;
end;

procedure TMainForm.FileNewItemClick(Sender: TObject);
begin
  NewDocument();
end;

procedure TMainForm.FileOpenItemClick(Sender: TObject);
var
  dialog : TOpenDialog;
  fileLocation : string;
  tab : TCustomTabSheet;
  activeDocument : TActiveDocument;
  i : integer;
begin
  dialog := TOpenDialog.Create(self);
  if dialog.Execute then begin
    fileLocation := dialog.FileName;
    for i := 0 to EditorsPageControl.PageCount - 2 do begin                     // - 2 to skip new tab sheet
      tab := TCustomTabSheet(EditorsPageControl.Pages[i]);
      activeDocument := tab.EditorFrame.ActiveDocument;
      if activeDocument.ActiveFileLocation.Equals(fileLocation) then begin
        EditorsPageControl.ActivePageIndex := i;
        exit;
      end;
    end;

    tab := TCustomTabSheet(EditorsPageControl.ActivePage);
    activeDocument := tab.EditorFrame.ActiveDocument;

    if ((activeDocument.ActiveFileLocation = '') and (activeDocument.Editor.Modified = true)) or (activeDocument.ActiveFileLocation <> '') then begin
      NewDocument();
    end;

    if EditorsPageControl.ActivePage is TCustomTabSheet then begin
      (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.RunCommand('File.Load "' + fileLocation + '"');
    end;
  end;
end;

procedure TMainForm.FileSaveAsItemClick(Sender: TObject);
begin
  if EditorsPageControl.ActivePage is TCustomTabSheet then begin
    (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.RunCommand('File.SaveAs');
  end;
end;

procedure TMainForm.FileSaveItemClick(Sender: TObject);
begin
  if EditorsPageControl.ActivePage is TCustomTabSheet then begin
    (EditorsPageControl.ActivePage as TCustomTabSheet).EditorFrame.RunCommand('File.Save');
  end;
end;

{$ENDREGION}

{$ENDREGION}

{$REGION 'CustomTabSheet'}

constructor TCustomTabSheet.Create(sheetOwner : TPageControl);
begin
  inherited Create(nil);
  _editorFrame := TEditorFrame.Create(self);
  _editorFrame.Align := alClient;
  _editorFrame.Parent := self;
  parent := sheetOwner;
end;

{$ENDREGION}

end.

