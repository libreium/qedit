unit uEditorFrame;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, ExtCtrls, ComCtrls, StdCtrls, SynEdit,
  uCommandUtils, uCommandProcessor, LCLType, uActiveDocument, SynHighlighterPas;

type

  { TEditorFrame }

  TEditorFrame = class(TFrame)
    BottomPanel: TPanel;
    CenterPanel: TPanel;
    ConsoleBox: TMemo;
    ConsolePage: TTabSheet;
    ConsoleScriptPageControl: TPageControl;
    NewScript: TTabSheet;
    SynEditor: TSynEdit;
    TerminalInput: TEdit;
    ActiveDocument : TActiveDocument;
    CommandProcessor : TCommandProcessor;
    CommandHistory : TStrings;
    procedure TerminalInputKeyPress(Sender: TObject; var Key: char);
    procedure TerminalInputUTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char
      );
  private
    function GetShortTitle() : string;
    function GetLongTitle() : string;
  public
    property ShortTitle : string read GetShortTitle;
    property LongTitle : string read GetLongTitle;
    function GetActiveDocument() : TActiveDocument;
    function GetProcessor() : TCommandProcessor;
    function RunCommand(command : string) : TCommandResult;
    procedure Test();
    constructor Create(AOwner : TComponent) ; override;
  end;

implementation

{$R *.lfm}

{$REGION 'Constructors'}

constructor TEditorFrame.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  CommandProcessor := TCommandProcessor.Create(ConsoleBox);
  ActiveDocument := TActiveDocument.Create(self.SynEditor);
end;

{$ENDREGION}

{$REGION 'Private Methods'}

function TEditorFrame.GetShortTitle() : string;
var
  titleText : string;
begin
  titleText := '';
  if SynEditor.Modified then begin
    titleText:= '*';
  end;

  if ActiveDocument.ActiveFileLocation ='' then begin
    titleText := titleText + 'New';
  end
  else begin
    titleText := titleText + ActiveDocument.FileName;
  end;

  Result := titleText;
end;

function TEditorFrame.GetLongTitle() : string;
var
  titleText : string;
begin
  titleText := '';
  if SynEditor.Modified then begin
    titleText:= '*';
  end;

  if ActiveDocument.ActiveFileLocation ='' then begin
    titleText := titleText + 'New';
  end
  else begin
    titleText := titleText + ActiveDocument.ActiveFileLocation;
  end;

  Result := titleText;
end;

{$ENDREGION}

{$REGION 'Public Methods'}

procedure TEditorFrame.Test();
var
  pas : TSynPasSyn;
begin
  pas := TSynPasSyn.Create(nil);

  SynEditor.Highlighter := pas;
end;

function TEditorFrame.GetActiveDocument() : TActiveDocument;
begin
  Result := ActiveDocument;
end;

function TEditorFrame.GetProcessor() : TCommandProcessor;
begin
  Result := CommandProcessor;
end;

function TEditorFrame.RunCommand(command : string) : TCommandResult;
begin
  ConsoleBox.Lines.Add(DateTimeToStr(Now, false)  + '  ' + command);

  Result := CommandProcessor.ProcessLine(command, ActiveDocument);

  if Result.Success = false then begin
    ConsoleBox.Lines.Add('Command Error : ' + Result.Message);
  end;
  ConsoleBox.VertScrollBar.Position := (ConsoleBox.VertScrollBar.Position + 1) * 100000;
end;

{$ENDREGION}

{$REGION 'Event Handlers'}

procedure TEditorFrame.TerminalInputKeyPress(Sender: TObject; var Key: char);
begin
  if (Key = #13) and (TerminalInput.Text <> '') then begin
    RunCommand(TerminalInput.Text);
    TerminalInput.Clear();
  end;
end;

procedure TEditorFrame.TerminalInputUTF8KeyPress(Sender: TObject;
  var UTF8Key: TUTF8Char);
begin

end;

{$ENDREGION}

end.

