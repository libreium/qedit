unit uScriptContext;

{$mode objfpc}{$H+}{$M+}

interface

uses
  Classes, SysUtils, fgl;

type
  TVarMap = specialize TFpgMap<string, string>;

  TScriptContext = class
    private
      _variables : TVarMap;
    public
      property Variables : TVarMap read _variables;

    public
      constructor Create;
  end;

implementation

{$REGION 'Constructors'}

constructor TScriptContext.Create();
begin
  _variables := TVarMap.Create;
  _variables.Add('test', 'test var'); //TODO remove when we can add variables by scripts
  _variables.Add('three', '3');
end;

{$ENDREGION}

end.

