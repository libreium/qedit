unit uCommandUtils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uGenericList, uCommandToken, uScriptContext;

type
  TTokenList = specialize TGList<TCommandToken>;

  TTopLevelCommands = class
    const
      AddLn = 'addln';
      Edit = 'edit';
      FileCmd = 'file'; //Cant call file as file is a keyworkd
  end;

  TCommandResult = record
      Success : boolean;
      Message : string;
      Line : integer;
  end;

  TCommandUtils = class
    class function CommandMatch(commandId : string; command : string) : boolean;
    class function StripCommandId(commandId : string; command : string) : string;
    class function ExtractString(out text : string) : TCommandResult;
    class function TokeniseCommand (command : string; scriptContext : TScriptContext;  out tokenList : TTokenList) : TCommandResult;
    class function TokensToStr(tokens : TTokenList) : string;
    class function ExtractString(command : string; context : TScriptContext; out commandOut : string; out token : TStringToken) : TCommandResult;
    class function ExtractToken(command : string; out commandOut : string; out token : TCommandToken) : TCommandResult;
    class function PopulateVariables(textIn : string; context : TScriptContext; out textOut : string) : TCommandResult;
  end;

implementation


{$REGION 'TCommandUtils'}

class function TCommandUtils.ExtractString(out text : string) : TCommandResult;
var
  tempText : string;
begin
  if (text.CountChar('"') = 2) then begin

  end
  else begin
    // Result := false;
    Result.Message := 'Invalid String ' + text;
  end;
end;

class function TCommandUtils.CommandMatch(commandId : string; command : string) : boolean;
begin
  while (command.StartsWith(' ')) and (command <> '') do begin
    command := command.Remove(0, 1);
  end;

  if command.StartsWith(commandId) then begin
    Result := true;
  end
  else begin
    Result := false;
  end;
end;

class function TCommandUtils.StripCommandId(commandId : string; command : string) : string;
begin
  while (command.StartsWith(' ')) and (command <> '') do begin
    command := command.Remove(0, 1);
  end;

  if command.StartsWith(commandId) then begin
    command := command.Remove(0, commandId.Length);
  end;

  Result := command;
end;

class function TCommandUtils.ExtractString(command : string; context : TScriptContext; out commandOut : string; out token : TStringToken) : TCommandResult;
var
  value : string;
  original : string;
  temp : string;
begin
  Result.Success := true;
  value := '';
  original := command;
  command := command.Remove(0,1);    //Remove opening " char

  while (command <> '') and (command.Chars[0] <> '"') do begin
    value := value + command.Chars[0];
    command := command.Remove(0,1);
  end;

  if (command = '')then begin
    Result.Message := 'String parse Invalid string ' + original;
    Result.Success := false;
    exit;
  end;

  if (command.Chars[0] <> '"')then begin
    Result.Message := 'Invalid string end " expected';
    Result.Success := false;
    exit;
  end;

  command := command.Remove(0,1);

  temp := value;
  Result := PopulateVariables(temp, context, value);

  if (Result.Success) then begin
    value := value.Replace('$$', '$');
  end;

  token := TStringToken.Create(value);
  commandOut := command;
end;

class function TCommandUtils.ExtractToken(command : string; out commandOut : string; out token : TCommandToken) : TCommandResult;
var
  value : string;
begin
  Result.Success := true;
  value := '';

  while (command <> '') and (command.Chars[0] <> ' ') and (command.Chars[0] <> '.') and (command.Chars[0] <> '"') do begin
    value := value + command.Chars[0];
    command := command.Remove(0, 1);
  end;

  if (command <> '') and ((command.Chars[0] = ' ') or (command.Chars[0] = '.')) then begin
    command := command.Remove(0, 1);
  end;

  if value <> '' then begin
    token := TCommandToken.Create(value);
  end;
  commandOut := command;
end;

class function TCommandUtils.TokeniseCommand(command : string; scriptContext : TScriptContext; out tokenList : TTokenList) : TCommandResult;
var
  i : integer;
  token : TCommandToken;
  stringToken : TStringToken;
  value : string;
  temp : string;
begin
  Result.Success := true;
  tokenList := TTokenList.Create;

  while (command <> '') and (Result.Success) do begin
    if command.Chars[0] = ' ' then begin
      command := command.Remove(0, 1);
    end
    else if command.Chars[0] = '"' then begin
      temp := command;
      Result := ExtractString(temp, scriptContext, command, stringToken);
      if (Result.Success) and (stringToken <> nil) then begin
        tokenList.Add(stringToken);
      end;
    end
    else begin
      temp := command;
      Result := ExtractToken(temp, command, token);
      if (Result.Success) and (token <> nil) then begin
        tokenList.Add(token);
      end;
    end;
  end;
end;

class function TCommandUtils.TokensToStr(tokens : TTokenList) : string;
var
  i : integer;
begin
  Result := '';
  for i := 0 to tokens.Count - 1 do begin
    Result := Result + ' ' + tokens.Items[i].Value;
  end;
end;

class function TCommandUtils.PopulateVariables(textIn : string; context : TScriptContext; out textOut : string) : TCommandResult;
var
  variableName : string;
  variableValue : string;
begin
  Result.Success := true;
  variableName := '';
  variableValue := '';
  textOut := '';

  while (textIn <> '') and (Result.Success) do begin
    if textIn.Chars[0] = '$' then begin
      if (textIn.Length > 1) and (textIn.Chars[1] <> '$') then begin
        textIn := textIn.Remove(0,1);

        while (textIn <> '') and (textIn.Chars[0] <> ' ') and (textIn.Chars[0] <> '$') do begin
          variableName := variableName + textIn.Chars[0];
          textIn := textIn.Remove(0,1);
        end;
        if context.Variables.TryGetData(variableName, variableValue) then begin
          textOut := textOut + variableValue;
        end
        else begin
          Result.Success := false;
          Result.Message := ' Undeclated variable $' + variableName;
        end;
        variableName := '';
        variableValue := '';
      end
      else if (textIn.Length > 1) and (textIn.Chars[1] = '$') then begin
        textOut := textOut + textIn.Chars[0];
        textIn := textIn.Remove(0,1);
        textOut := textOut + textIn.Chars[0];
        textIn := textIn.Remove(0,1);
      end;
    end
    else begin
      textOut := textOut + textIn.Chars[0];
      textIn := textIn.Remove(0,1);
    end;
  end;
end;

{$ENDREGION}

end.

