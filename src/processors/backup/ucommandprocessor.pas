unit uCommandProcessor;

{$mode objfpc}{$H+}{$M+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  uCommandUtils, uFileCommandProcessor, uEditProcessor,
  uCommandToken, uScriptContext, uActiveDocument;

type
  TTaskThread = class(TThread)
    public
      procedure Execute; override;
  end;

  TCommandProcessor = class
    OutputBox : TMemo;
    ScriptContext : TScriptContext;
    TaskThread : TTaskThread;
    private
      function AddLn(tokens : TTokenList; text : TStrings) : TCommandResult;
      function VariableAccessCommand(tokens : TTokenList) : TCommandResult;
    public
      constructor Create(memoBox : TMemo);
    public
      //function ProcessCommands(commands : TStrings; text : TStrings) : TCommandResult;
      function ProcessLine(command : String; activeDocument : TActiveDocument) : TCommandResult;
      procedure Run(command : string);
      procedure OnTerminate(Sender : TObject);
  end;

implementation

{$REGION 'TCommandProcessor'}

{$REGION 'Constructor / destructors'}

constructor TCommandProcessor.Create(memoBox : TMemo);
begin
  OutputBox := memoBox;
  ScriptContext := TScriptContext.Create;
end;

{$ENDREGION}


{$REGION 'Public Methods'}

procedure TCommandProcessor.OnTerminate(Sender : TObject);
begin

end;

procedure TCommandProcessor.Run(command : string);
begin
  TaskThread := TTaskThread.Create(false, DefaultStackSize);
  TaskThread.OnTerminate := @OnTerminate;
end;

function TCommandProcessor.ProcessLine(command : String; activeDocument : TActiveDocument) : TCommandResult;
var
  return : TCommandResult;
  tokenList : TTokenList;
  i : integer;
begin
  Result.Success := false;
  Result.Message := 'Unknown command';

  Result := TCommandUtils.TokeniseCommand(command, ScriptContext, tokenList);

  if Result.Success = false then begin
    exit;
  end;

  if (Result.Success) and (tokenList <> nil) and (tokenList.Count > 0) then begin
    if (tokenList.Items[0].Value.StartsWith('$')) then begin
      Result := VariableAccessCommand(tokenList);
      exit;
    end;
  end;

  if (Result.Success) and (tokenList <> nil) and (tokenList.Count > 0) then begin
    if (tokenList.Items[0].Value = TTopLevelCommands.Edit) then begin
      tokenList.RemoveFirst();
      Result := TEditProcessor.ProcessLine(tokenList, activeDocument);
      exit;
    end;

    if (tokenList.Items[0].Value = 'addln') then begin
      tokenList.RemoveFirst();
      Result := AddLn(tokenList, activeDocument.Editor.Lines);
      exit;
    end;

    if (tokenList.Items[0].Value = 'test') then begin
      //tokenList.RemoveFirst();
      for i := 0 to 100 do begin
        activeDocument.Editor.Lines.Add('test ' + IntToStr(i));
        Application.ProcessMessages;
        Sleep(200);
      end;
      exit;
    end;

    //if (tokenList.Items[0].Value = 'file') then begin
    //  tokenList.RemoveFirst();
    //end;
  end;

end;

{$ENDREGION}

{$ENDREGION}

{$REGION 'Private Methods'}

function TCommandProcessor.AddLn(tokens : TTokenList; text : TStrings) : TCommandResult;
var
  token : TStringToken;
begin
  if (tokens.Count = 0) then begin
    Result.Success := false;
    Result.Message := 'AddLn String expected';
  end
  else if (tokens.Count = 1) and (tokens.Items[0] is TStringToken) then begin
    token := TStringToken(tokens.Items[0]);
    text.Add(token.Value);
    Result.Success := true;
  end;
end;

function TCommandProcessor.VariableAccessCommand(tokens : TTokenList) : TCommandResult;
var
  variableName : string;
  variableValue : string;
  i : integer;
  unexpected : string;
begin
  variableName := tokens.Items[0].Value.Replace('$', '');
  if (tokens.Count = 1) then begin
    if (ScriptContext.Variables.TryGetData(variableName, variableValue)) then begin
      Result.Success := true;
      OutputBox.Lines.Add('$' + variableName + ' = "' + variableValue + '"');
    end
    else begin
      Result.Success := true;
      ScriptContext.Variables.Add(variableName, '');
    end;
  end
  else if (tokens.Count = 2) then begin
    //Should variables only accept string as parameter?
    //if (tokens.Items[1] is TStringToken) then begin
      ScriptContext.Variables.AddOrSetData(variableName, tokens.Items[1].Value);
      Result.Success := true;
    //end
    //else begin
    //  Result.Success := false;
    //  Result.Message := 'Variable expected string';
    //end;
  end
  else begin
    Result.Success := false;
    unexpected := '';
    for i := 1 to tokens.Count - 1 do begin
      unexpected := unexpected + ' ' + tokens.Items[i].Value;
      Result.Message := 'Set variable expected value, got "' + unexpected + '". Usage $var "value"';
    end;
  end;
end;

{$ENDREGION}

{$REGION 'TTaskThread'}

procedure TTaskThread.Execute();
begin

end;

{$ENDREGION}

end.

