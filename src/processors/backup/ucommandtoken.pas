unit uCommandToken;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TCommandToken = class
    private
      _value : string;
      function ReadValue() : string; virtual;
    public
      constructor Create(value : string);
    public
      property Value : string read ReadValue;
  end;

  TStringToken = class(TCommandToken)
    private
      function ReadValue() : string; override;
  end;

implementation


{$REGION 'TCommandToken'}

{$REGION 'Constructors'}

constructor TCommandToken.Create(value : string);
begin
  _value := value;
end;

{$ENDREGION}

function TCommandToken.ReadValue : string;
begin
  Result := _value.ToLower;
end;

{$ENDREGION}

{$REGION 'TStringToken'}

function TStringToken.ReadValue() : string;
begin
  Result := _value;
end;

{$ENDREGION}
end.

