unit uEditProcessor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uCommandToken, uCommandUtils, uActiveDocument;

type
  TEditCommands = class
    const
      Undo = 'undo';
      Redo = 'redo';
  end;

  TEditProcessor = class
    private
      class function Undo(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
      class function Redo(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
    public
      class function ProcessLine(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
  end;

implementation

{$REGION 'Public Methods'}

class function TEditProcessor.ProcessLine(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
begin
  Result.Success := true;

  if (tokens <> nil) and (tokens.Count > 0) then begin
    if tokens.Items[0].Value = TEditCommands.Undo then begin
      Result := Undo(tokens, activeDocument);
    end
    else if tokens.Items[0].Value = TEditCommands.Redo then begin
      Result := Redo(tokens, activeDocument);
    end
    else begin
      Result.Success := false;
      Result.Message := 'Edit unexpected: ' + tokens.Items[0].Value;
    end;
  end
  else begin
    Result.Success := false;
    Result.Message := 'Edit expected Undo, Redo, ...';
  end;
end;

{$ENDREGION}

{$REGION 'Private Methods'}

class function TEditProcessor.Redo(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
var
  n : integer;
  i : integer;
  unexpected : string;
begin
  Result.Success := true;

  if tokens.Count = 1 then begin                                                //command Edit.Redo
    activeDocument.Editor.Redo();
  end
  else if tokens.Count = 2 then begin                                           //Attempt to run Edit.undo n
    if TryStrToInt(tokens.Items[1].Value, n) then begin
      for i := 1 to n do begin
        activeDocument.Editor.Redo();
      end;
    end
    else begin
       Result.Success := false;
       Result.Message := 'Edit.Redo unexpected "' + tokens.Items[1].Value + '". Usage Edit.Redo N';
    end;
  end
  else begin                                                                    //Unexpected parameters
    Result.Success := false;
    tokens.RemoveFirst();
    unexpected := TCommandUtils.TokensToStr(tokens);
    Result.Message := 'Edit.Undo unexpected "' + unexpected + '". Usage Edit.undo N';
  end;
end;

class function TEditProcessor.Undo(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
var
  n : integer;
  i : integer;
  unexpected : string;
begin
  Result.Success := true;

  if tokens.Count = 1 then begin                                                //command Edit.Undo
    activeDocument.Editor.Undo();
  end
  else if tokens.Count = 2 then begin                                           //Attempt to run Edit.undo n
    if TryStrToInt(tokens.Items[1].Value, n) then begin
      for i := 1 to n do begin
        activeDocument.Editor.Undo();
      end;
    end
    else begin
       Result.Success := false;
       Result.Message := 'Edit.Undo unexpected "' + tokens.Items[1].Value + '". Usage Edit.undo N';
    end;
  end
  else begin                                                                    //Unexpected parameters
    Result.Success := false;
    tokens.RemoveFirst();
    unexpected := TCommandUtils.TokensToStr(tokens);
    Result.Message := 'Edit.Undo unexpected "' + unexpected + '". Usage Edit.undo N';
  end;
end;

{$ENDREGION}

end.

