unit uFileCommandProcessor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uCommandUtils, uActiveDocument, uCommandToken, Dialogs, lcl;
type
  TFileCommands = class
    const
      Clear = 'clear';
      Close = 'close';
      Load = 'load';
      Save = 'save';
      SaveAs = 'saveas';
      Reload = 'reload';
  end;

  TFileCommandProcessor = class
    public
      class function ProcessLine(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;

    private
      class function Clear(text : TStrings) : TCommandResult;
      class function Close(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
      class function Load(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
      class function Save(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
      class function SaveAs(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
      class function Reload(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
  end;

implementation

{$REGION 'Public Methods'}

class function TFileCommandProcessor.ProcessLine(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
begin
  Result.Success := true;

  if (tokens <> nil) and (tokens.Count > 0) then begin
    if tokens.Items[0].Value = TFileCommands.SaveAs then begin
      tokens.RemoveFirst();
      Result := SaveAs(tokens, activeDocument);
    end
    else if tokens.Items[0].Value = TFileCommands.Save then begin
      tokens.RemoveFirst();
      Result := Save(tokens, activeDocument);
    end
    else if tokens.Items[0].Value = TFileCommands.Load then begin
      tokens.RemoveFirst();
      Result := Load(tokens, activeDocument);
    end
    else if tokens.Items[0].Value = TFileCommands.Reload then begin
      tokens.RemoveFirst();
      Result := Reload(tokens, activeDocument);
    end
    else if tokens.Items[0].Value = TFileCommands.Close then begin
      tokens.RemoveFirst();
      Result := Close(tokens, activeDocument);
    end
    else if tokens.Items[0].Value = TFileCommands.Clear then begin
      Result := Clear(activeDocument.Editor.Lines);
    end
    else begin
      Result.Success := false;
      Result.Message := 'File unexpected: ' + tokens.Items[0].Value;
    end;
  end
  else begin
    Result.Success := false;
    Result.Message := 'Edit expected Undo, Redo, ...';
  end;
end;

{$ENDREGION}

{$REGION 'Command Methods'}

class function TFileCommandProcessor.Clear(text : TStrings) : TCommandResult;
begin
  Result.Success := true;

  text.Clear;
end;

class function TFileCommandProcessor.Close(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
var
  unexpected : string;
  ans : integer;
  buttons : TMsgDlgButtons;
begin
  Result.Success := true;
  if tokens.Count = 0 then begin
    if activeDocument.Editor.Modified then begin
      if activeDocument.ActiveFileLocation = '' then begin
        ans := MessageDlg('Save file', 'Do you want to save changes made to new file?', TMsgDlgType.mtConfirmation, mbYesNoCancel, '');
      end
      else begin
        ans := MessageDlg('Save file', 'Do you want to save changes made to ' + ExtractFileName(activeDocument.ActiveFileLocation) + '?', TMsgDlgType.mtConfirmation, mbYesNoCancel, '');
      end;

      if ans = 6 then begin
        Result := Save(tokens, activeDocument);
      end
      else if ans = 2 then begin
        Result.Success := false
        Result.Message := 'File.Close canceled by user';
      end;

      if Result.Success then begin
        activeDocument.Clear();
      end;
    end;
  end
  else begin
    Result.Success := false;
    unexpected := TCommandUtils.TokensToStr(tokens);
    Result.Message := 'File.Close unexpected "' + unexpected + '". Usage File.Close';
  end;
end;

class function TFileCommandProcessor.Load(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
var
  fileLocation : string;
  unexpected : string;
begin
  if (tokens.Count = 1) and (tokens.Items[0] is TStringToken) then begin
    fileLocation := tokens.Items[0].Value;
    Result := activeDocument.Load(fileLocation);
  end
  else begin
    Result.Success := false;
    unexpected := TCommandUtils.TokensToStr(tokens);
    Result.Message := 'File.Load unexpected "' + unexpected + '". Usage File.Load "filename"';
  end;
end;

class function TFileCommandProcessor.Save(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
var
  unexpected : string;
begin
  if tokens.Count = 0 then begin
    if activeDocument.ActiveFileLocation <> '' then begin
      Result := activeDocument.Save();
    end
    else begin
      Result := SaveAs(tokens, activeDocument);
    end;
  end
  else begin
    Result.Success := false;
    unexpected := TCommandUtils.TokensToStr(tokens);
    Result.Message := 'File.Save unexpected "' + unexpected + '". Usage File.Save, use File.SaveAs for new file';
  end;
end;

class function TFileCommandProcessor.SaveAs(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
var
  unexpected : string;
  fileLocaiton : string;
  dialog : TSaveDialog;
begin
  if tokens.Count = 1 then begin
    if tokens.Items[0] is TStringToken then begin
      fileLocaiton := tokens.Items[0].Value;
      activeDocument.SaveAs(fileLocaiton);
    end
    else begin
      Result.Success := false;
      Result.Message := 'File.SaveAs expected string, got "' + tokens.Items[0].Value + '"';
    end;
  end
  else if tokens.Count = 0 then begin
    dialog := TSaveDialog.Create(nil);
    if dialog.Execute then begin
      fileLocaiton := dialog.FileName;
      Result := activeDocument.SaveAs(fileLocaiton);
    end
    else begin
      fileLocaiton := '';
      Result.Success := false;
      Result.Message := 'SaveAs canceled by user';
    end;
  end
  else begin
    Result.Success := false;
    unexpected := TCommandUtils.TokensToStr(tokens);
    Result.Message := 'File.SaveAs unexpected "' + unexpected + '". Usage File.SaveAs or File.SaveAs "filename"';
  end;
end;

class function TFileCommandProcessor.Reload(tokens : TTokenList; activeDocument : TActiveDocument) : TCommandResult;
var
  unexpected : string;
begin
  if tokens.Count = 0 then begin
    Result := activeDocument.Reload();
  end
  else begin
    Result.Success := false;
    unexpected := TCommandUtils.TokensToStr(tokens);
    Result.Message := 'File.Reload unexpected "' + unexpected + '". Usage File.Reload';
  end;
end;

{$ENDREGION}
end.

