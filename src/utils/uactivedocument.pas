unit uActiveDocument;

{$mode objfpc}{$H+}{$M+}

interface

uses
  Classes, SysUtils, SynEdit, uCommandUtils;

type
  TActiveDocument = class
    private
      _editor : TSynEdit;
      function GetFileName() : string;
    public
      ActiveFileLocation : string;
      property Editor : TSynEdit read _editor;
      property FileName : string read GetFileName;
    public
      constructor Create(activeEditor : TSynEdit);
      procedure Clear();
      function Load(fileLocation : string) : TCommandResult;
      function Save() : TCommandResult;
      function SaveAs(fileLocation : string) : TCommandResult;
      function Reload() : TCommandResult;
  end;

implementation

{$REGION 'Constructors'}

constructor TActiveDocument.Create(activeEditor : TSynEdit);
begin
  _editor := activeEditor;
end;

{$ENDREGION}


{$REGION 'Public Methods'}

procedure TActiveDocument.Clear();
begin
  ActiveFileLocation := '';
  Editor.Lines.Clear();
  Editor.Modified := false;
end;

function TActiveDocument.Load(fileLocation : string) : TCommandResult;
var
  fileStream : TFileStream;
  line : string;
  text : string;
  currentByte : byte;
begin
  Result.Success := true;
  try
    if FileExists(fileLocation) then begin
      ActiveFileLocation := fileLocation;
      fileStream := TFileStream.Create(ActiveFileLocation, fmOpenRead);
      fileStream.Position := 0;
      line := '';

      while fileStream.Position <> fileStream.Size do begin
        currentByte := fileStream.ReadByte;
        line := line + Char(currentByte);
      end;
      Editor.Text:= line;
      Editor.Modified := false;
      fileStream.Free;
    end
    else begin
      Result.Success := false;
      Result.Message := 'File.Load error, file ' + fileLocation + ' does not exist!';
    end;
  except
    begin
      Result.Success := false;
      REsult.Message := 'Load error';
    end;
  end;
end;

function TActiveDocument.Save() : TCommandResult;
var
  fileStream : TFileStream;
  i : integer;
  line : string;
begin
  try
    if FileExists(ActiveFileLocation) then begin
      fileStream := TFileStream.Create(ActiveFileLocation, fmOpenReadWrite);
      fileStream.Size:=0;
      fileStream.Position := 0;

      for i := 0 to _editor.Lines.Count - 1 do begin
        line := _editor.Lines[i];
        if i < _editor.Lines.Count - 1  then begin
          line := line + #13#10
        end;
        fileStream.Write(line[1], Length(line));
      end;
      fileStream.Free;
    end
    else begin
      fileStream := TFileStream.Create(ActiveFileLocation, fmCreate);
      fileStream.Size:=0;
      fileStream.Position := 0;

      for i := 0 to _editor.Lines.Count - 1 do begin
        line := _editor.Lines[i];
        if i < _editor.Lines.Count - 1  then begin
          line := line + #13#10
        end;
        fileStream.Write(line[1], Length(line));
      end;
      fileStream.Free;
    end;

    Editor.MarkTextAsSaved();
    Editor.Modified := false;
  except
    begin
      Result.Success := false;
      REsult.Message := 'Save error';
    end;
  end;
end;

function TActiveDocument.SaveAs(fileLocation : string) : TCommandResult;
begin
  ActiveFileLocation := fileLocation;
  Result := Save();
end;

function TActiveDocument.Reload() : TCommandResult;
begin
  if ActiveFileLocation <> '' then begin
    Result := Load(ActiveFileLocation);
  end
  else begin
    Result.Success := false;
    Result.Message := 'File.Reload error, no active file open';
  end;
end;

{$ENDREGION}

{$REGION 'Public Methods'}


function TActiveDocument.GetFileName() : string;
var
  name : string;
begin
  name := ExtractFileName(ActiveFileLocation);
  Result := name;
end;

{$ENDREGION}

end.

