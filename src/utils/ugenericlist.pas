unit uGenericList;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  generic TGList<T> = class
    public
      Items : array of T;
    public
      procedure Add(item : T);
      constructor Create;
      procedure RemoveFirst();
    private
      function GetCount() : Integer;
    public
      property Count : Integer Read GetCount;
  end;

implementation

{$REGION 'constructors'}

constructor TGList.Create();
begin

end;

{$ENDREGION}

{$REGION 'public methods'}

procedure TGList.Add(item : T);
begin
  SetLength(Items, Length(Items) + 1);
  Items[Length(Items) - 1] := item;
end;

procedure TGList.RemoveFirst();
var
  tempList : array of T;
  i : integer;
begin
  if (GetCount() > 1) then begin
    for i := 1 to GetCount() - 1 do begin
      Items[i-1] := Items[i];
    end;
    SetLength(Items, GetCount() - 1);
  end
  else if (GetCount() = 1) then begin
    SetLength(Items, 0);
  end;
end;

{$ENDREGION}

{$REGION 'private methods'}

function TGList.GetCount() : Integer;
begin
  Result := Length(Items);
end;

{$ENDREGION}

end.

